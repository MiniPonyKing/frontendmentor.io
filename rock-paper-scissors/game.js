window.onload = function() {
};

var handChoices = {
    PAPER: 'paper',
    SCISSORS: 'scissors',
    ROCK: 'rock',
}
var playerHandChoice = handChoices[0];
var houseHandChoice = handChoices[0];

function handClick(handChoice) {
    playerHandChoice = handChoice;
    houseHandChoice = generateComputerChoice();
    console.log(houseHandChoice);
    showBattle();
}

function showBattle() {
    hands.style.display = 'none';
    battle.style.display = 'flex';
    player.classList.add(`hand-${playerHandChoice}`);
    house.classList.add(`hand-${houseHandChoice}`);
    let playerImage = player.querySelector('img');
    playerImage.src = `./images/icon-${playerHandChoice}.svg`;
    let houseImage = house.querySelector('img');
    houseImage.src = `./images/icon-${houseHandChoice}.svg`;
}

function generateComputerChoice() {
    let randomChoice = Math.floor(Math.random() * 3);
    return Object.values(handChoices)[randomChoice];
}