var savedIndexForGalleryOpening = 0;
var itemsInCart = [];

window.onload = function () {
    initializeThumbnails();
    initializeGallery();
    initializeQuantityButtons();
    initializeGalleryArrows();
    initializeAddButton();
    initializeCartButton();
    initializeImageArrows();
    // If I had to go back, I'll never do like this, I would rather just specified onclick event on each
    // single HTML elements.. I just forgotted it was a thing, sry
};

function initializeThumbnails() {
    document.querySelectorAll('.thumbnail').forEach(t => {
        t.addEventListener('click', function () {
            t.parentElement.parentElement.querySelector('.thumbnail.active').classList.remove('active');
            t.classList.add('active');
            let index = Array.from(t.parentElement.children).indexOf(t);
            if (t.parentElement.parentElement == thumbnails)
                savedIndexForGalleryOpening = Array.from(t.parentElement.children).indexOf(t);
            t.parentElement.parentElement.querySelector('.preview').src = `./images/image-product-${index + 1}.jpg`;
        });
    });
}

function initializeQuantityButtons() {
    quantityButtons.querySelectorAll('.quantity-button').forEach(button => {
        button.addEventListener('click', function (e) {
            let operation = e.srcElement.innerText;
            let result = eval(`+quantity.value ${operation} 1`);
            result = result > 0 ? result : 0;
            quantity.value = result;
        });
    });
}

function initializeGallery() {
    bigImage.addEventListener('click', function () {
        openGallery();
    });
    closer.addEventListener('click', function () {
        closeGallery();
    });
}

function openGallery() {
    galleryImage.src = `./images/image-product-${savedIndexForGalleryOpening + 1}.jpg`;
    gallery.querySelector('.thumbnail.active').classList.remove('active');
    gallery.querySelector(`.thumbnail:nth-child(${savedIndexForGalleryOpening + 1})`).classList.add('active');
    gallery.style.display = 'flex';
}

function closeGallery() {
    gallery.style.display = 'none';
}

function initializeGalleryArrows() {
    gallery.querySelectorAll('.gallery-arrow-container').forEach(arrow => {
        arrow.addEventListener('click', function (e) {
            let imagesCount = gallery.querySelector('.thumbnails-container').children.length;
            let index = galleryImage.src.split('-').pop().replace('.jpg', '');
            console.log(arrow.id);
            if (arrow.id == 'gallery-previous-button')
                index--;
            if (arrow.id == 'gallery-next-button')
                index++;
            let moduledIndex = ((index - 1) % imagesCount) + 1;
            moduledIndex = moduledIndex == 0 ? 4 : moduledIndex;
            galleryImage.src = `./images/image-product-${moduledIndex}.jpg`;

            gallery.querySelector('.thumbnail.active').classList.remove('active');
            gallery.querySelector(`.thumbnail:nth-child(${moduledIndex})`).classList.add('active');
        });
    });
}

// TODO: merge with above..
function initializeImageArrows() {
    thumbnails.querySelectorAll('.gallery-arrow-container').forEach(arrow => {
        arrow.addEventListener('click', function (e) {
            let imagesCount = gallery.querySelector('.thumbnails-container').children.length;
            let index = bigImage.src.split('-').pop().replace('.jpg', '');
            console.log(arrow.id);
            if (arrow.id == 'big-image-previous-button')
                index--;
            if (arrow.id == 'big-image-next-button')
                index++;
            let moduledIndex = ((index - 1) % imagesCount) + 1;
            moduledIndex = moduledIndex == 0 ? 4 : moduledIndex;
            bigImage.src = `./images/image-product-${moduledIndex}.jpg`;
        });
    });
}

function initializeCartButton() {
    cartButton.addEventListener('click', function () {
        cart.style.display = cart.style.display != 'flex' ? 'flex' : 'none';
    });
}

window.addEventListener('click', function (e) {
    if (!e.target.id.toLowerCase().includes('cart'))
        cart.style.display = 'none';
})

function initializeAddButton() {
    addButton.addEventListener('click', function () {
        if (quantity.value == 0) {
            if (quantityErrorMessage.style.display == 'flex') return;
            quantityErrorMessage.style.display = 'flex';
            setTimeout(() => {
                quantityErrorMessage.style.display = 'none';
            }, 3000);
            return;
        }

        itemsInCart.push({ // Totally unecessary, I played as I was React 
            id: 1,
            image: './images/image-product-1.jpg', // imagining this can handle diffrent products
            description: 'Fall Limited Edition Sneakers',
            price: '125',
            quantity: quantity.value
        });
        updateCart();
        updateCartCounter();
    });
}

// Totally unecessary, I played as I was React
function updateCart() { 
    cartItems.innerHTML = '';
    if (!itemsInCart.length) {
        emptyCartMessage.style.display = 'flex';
        checkoutButton.style.display = 'none';
        return;
    }

    var productsById = [];
    itemsInCart.forEach(function (item) {
        let existingItem = productsById.find(i => i.id == item.id);
        if (!existingItem) {
            productsById.push(item);
            return;
        }

        existingItem.quantity = +existingItem.quantity + +item.quantity;
    });
    productsById.forEach(product => {
        addProductTemplate(product);
    });
    
    emptyCartMessage.style.display = 'none';
    checkoutButton.style.display = 'flex';
}

// Totally unecessary, I played as I was React
function addProductTemplate(product) {
    let total = product.price*product.quantity;
    let template = `
        <div id="cart-info">
        <img id="cart-image" src="${product.image}" alt="product-image"/>
        <div class="flex-column-gap-05 pe-none">
        <div>${product.description}</div>
        <div class="flex ai-center">
            <span class="greyish">$${product.price}.00</span>
            <span class="greyish">x ${product.quantity}</span>
            <span class="bold">$${total}</span>
        </div>
        </div>
        <img id="deleteCartProduct" src="./images/icon-delete.svg" alt="delete">
    </div>`;
    cartItems.innerHTML += template;
    deleteCartProduct.addEventListener('click', function() {
        itemsInCart = []; // TODO: we only have one kind of product (one id)..
        updateCart();
        updateCartCounter();
    })    
}

function updateCartCounter() {
    if (!itemsInCart.length) {
        cartCounter.innerText = 0;
        cartCounter.style.display = 'none';
        return;
    }

    cartCounter.innerText = itemsInCart.reduce(function(a, b){ return +a + +b.quantity }, 0);
    cartCounter.style.display = 'flex';
}

function openMenu() {
    // menu.style.display = 'flex'; // TODO: for now like this, we need to do opening animation
    menu.style.animation = 'open-menu 1s cubic-bezier(0.075, 0.82, 0.165, 1) forwards';
    hider.style.display = 'flex';
}

function closeMenu() {
    // menu.style.animationDirection = 'reverse';
    menu.style.animation = 'close-menu 1s cubic-bezier(0.075, 0.82, 0.165, 1) forwards';
    hider.style.display = 'none';
}